import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Patch,
  Delete,
  UsePipes,
  ValidationPipe,
  ParseUUIDPipe,
} from '@nestjs/common';
import { Task } from './task.model';
import { TaskService } from './task.service';
import { CreateTaskDto } from './dtos/create-task.dto';
import { UpdateTaskDto } from './dtos/update-task.dto';
import {
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { plainToInstance } from 'class-transformer';
import { UUID } from 'crypto';

@ApiTags('tasks')
@Controller('task')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @ApiOperation({ summary: 'Get all tasks' })
  @ApiResponse({ status: 200, description: 'Return all tasks', type: [Task] })
  @Get()
  async findAll(): Promise<Task[]> {
    return await this.taskService.findAll();
  }

  @ApiOperation({ summary: 'Get a task by ID' })
  @ApiResponse({ status: 200, description: 'Return the task', type: Task })
  @ApiNotFoundResponse({ description: 'Task not found' })
  @Get(':id')
  async findOne(@Param('id', ParseUUIDPipe) id: UUID): Promise<Task> {
    return this.taskService.findOne(id);
  }

  @ApiOperation({ summary: 'Create a new task' })
  @ApiResponse({
    status: 201,
    description: 'The task has been successfully created',
    type: Task,
  })
  @ApiBadRequestResponse({ description: 'Invalid task data' })
  @Post()
  @UsePipes(new ValidationPipe({ whitelist: true, forbidNonWhitelisted: true }))
  async create(@Body() createTaskDto: CreateTaskDto): Promise<Task> {
    return await this.taskService.create(createTaskDto);
  }

  @ApiOperation({ summary: 'Update a task' })
  @ApiResponse({
    status: 200,
    description: 'The task has been successfully updated',
    type: Task,
  })
  @ApiNotFoundResponse({ description: 'Task not found' })
  @Patch(':id')
  @UsePipes(new ValidationPipe({ whitelist: true, forbidNonWhitelisted: true }))
  async update(
    @Param('id', ParseUUIDPipe) id: UUID,
    @Body() updateTaskDto: UpdateTaskDto,
  ): Promise<Task> {
    const task = plainToInstance(Task, updateTaskDto);
    return await this.taskService.update(id, task);
  }

  @ApiOperation({ summary: 'Delete a task' })
  @ApiResponse({
    status: 204,
    description: 'The task has been successfully deleted',
  })
  @ApiNotFoundResponse({ description: 'Task not found' })
  @Delete(':id')
  async delete(@Param('id', ParseUUIDPipe) id: UUID): Promise<void> {
    await this.taskService.delete(id);
  }
}
