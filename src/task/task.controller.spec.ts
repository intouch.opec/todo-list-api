import { Test, TestingModule } from '@nestjs/testing';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';
import { CreateTaskDto } from './dtos/create-task.dto';
import { UpdateTaskDto } from './dtos/update-task.dto';
import { NotFoundException, BadRequestException } from '@nestjs/common';
import { UUID, randomUUID } from 'crypto';

describe('TaskController', () => {
  let controller: TaskController;
  let service: TaskService;
  const uuid = randomUUID();
  const uuidNotFound = randomUUID();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TaskController],
      providers: [
        {
          provide: TaskService,
          useValue: {
            findAll: jest.fn().mockResolvedValue([]),
            findOne: jest.fn().mockImplementation((id: UUID) => {
              if (id === uuidNotFound) {
                throw new NotFoundException(`Task with id ${id} not found`);
              }
              return { id, title: 'Test Task' };
            }),
            create: jest.fn().mockImplementation((task: CreateTaskDto) => {
              if (!task.title) {
                throw new BadRequestException('Task title is required');
              }
              return { id: randomUUID(), ...task };
            }),
            update: jest
              .fn()
              .mockImplementation((id: UUID, task: UpdateTaskDto) => {
                if (id === uuidNotFound) {
                  throw new NotFoundException(`Task with id ${id} not found`);
                }
                return { id, ...task };
              }),
            delete: jest.fn().mockImplementation((id: UUID) => {
              if (id === uuidNotFound) {
                throw new NotFoundException(`Task with id ${id} not found`);
              }
              return;
            }),
          },
        },
      ],
    }).compile();

    controller = module.get<TaskController>(TaskController);
    service = module.get<TaskService>(TaskService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return an array of tasks', async () => {
    await expect(controller.findAll()).resolves.toEqual([]);
  });

  it('should return a task by ID', async () => {
    await expect(controller.findOne(uuid)).resolves.toEqual({
      id: uuid,
      title: 'Test Task',
    });
  });

  it('should create a new task', async () => {
    const newTask: CreateTaskDto = {
      title: 'New Task',
      description: 'New Task Description',
    };
    await expect(controller.create(newTask)).resolves.toEqual({
      id: expect.any(String),
      ...newTask,
    });
  });

  it('should return 400 when creating a task without a title', async () => {
    const newTask: CreateTaskDto = {
      description: 'New Task Description',
      title: '',
    };
    await expect(controller.create(newTask)).rejects.toThrow(
      BadRequestException,
    );
  });

  it('should update a task', async () => {
    const updatedTask: UpdateTaskDto = {
      title: 'Updated Task',
      description: 'Updated Task Description',
    };
    await expect(controller.update(uuid, updatedTask)).resolves.toEqual({
      id: uuid,
      ...updatedTask,
    });
  });

  it('should return 404 when updating a non-existent task', async () => {
    const updatedTask: UpdateTaskDto = {
      title: 'Updated Task',
      description: 'Updated Task Description',
    };
    await expect(
      controller.update(uuidNotFound, updatedTask),
    ).rejects.toThrow(NotFoundException);
  });

  it('should delete a task', async () => {
    await expect(controller.delete(uuid)).resolves.toBeUndefined();
  });

  it('should return 404 when deleting a non-existent task', async () => {
    await expect(
      controller.delete(uuidNotFound),
    ).rejects.toThrow(NotFoundException);
  });
});
