import { UUID } from "crypto";

export class Task {
  id: UUID;
  title: string;
  description: string;
  completed: boolean;
  completedAt?: Date | null;
  createdAt: Date;
  updatedAt?: Date;
}
