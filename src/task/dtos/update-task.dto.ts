import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsOptional, IsBoolean } from 'class-validator';

export class UpdateTaskDto {
  @ApiProperty({ description: 'The title of the todo item', required: false })
  @IsOptional()
  @IsString()
  title?: string;

  @ApiProperty({ description: 'The description of the todo item', required: false })
  @IsOptional()
  @IsString()
  description?: string;

  @ApiProperty({ description: 'The completion status of the todo item', required: false })
  @IsOptional()
  @IsBoolean()
  completed?: boolean;
}
