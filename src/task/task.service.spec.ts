import { Test, TestingModule } from '@nestjs/testing';
import { TaskService } from './task.service';
import { NotFoundException, BadRequestException } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { Task } from './task.model';

describe('TaskService', () => {
  let taskService: TaskService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TaskService],
    }).compile();

    taskService = module.get<TaskService>(TaskService);
  });

  describe('findOne', () => {
    it('should throw NotFoundException for non-existent task', () => {
      const nonExistentId = randomUUID(); 
      expect(() => taskService.findOne(nonExistentId)).toThrow(NotFoundException);
    });

    it('should return task when found', async () => {
      const existingId = (await taskService.create({ title: 'New Task' })).id;
      const foundTask = await taskService.findOne(existingId);
      expect(foundTask).toBeDefined();
      expect(foundTask.id).toEqual(existingId);
    });
  });

  describe('create', () => {
    it('should throw BadRequestException if title is missing', () => {
      const taskWithoutTitle = { title: '' };
      expect(() => taskService.create(taskWithoutTitle)).toThrow(BadRequestException);
    });

    it('should create and return task when title is provided',async() => {
      const newTask = { title: 'New Task' };
      const createdTask = await taskService.create(newTask);
      expect(createdTask).toBeDefined();
      expect(createdTask.title).toEqual(newTask.title);
    });
  });

  describe('update', () => {
    it('should throw NotFoundException for non-existent task', () => {
      const nonExistentId = randomUUID();
      const taskUpdate = { title: 'Updated Title' } as Task;
      expect(() => taskService.update(nonExistentId, taskUpdate)).toThrow(NotFoundException);
    });

    it('should update and return task when found', async () => {
      const existingTask = await taskService.create({ title: 'Task to Update' });
      const taskUpdate = { title: 'Updated Title' } as Task;
      const updatedTask = await taskService.update(existingTask.id, taskUpdate);
      expect(updatedTask).toBeDefined();
      expect(updatedTask.id).toEqual(existingTask.id);
      expect(updatedTask.title).toEqual(taskUpdate.title);
    });
  });

  describe('delete', () => {
    it('should throw NotFoundException for non-existent task', () => {
      const nonExistentId = randomUUID();
      expect(() => taskService.delete(nonExistentId)).toThrow(NotFoundException);
    });

    it('should delete and return deleted task when found', async () => {
      const existingTask = await taskService.create({ title: 'Task to Delete' });
      const deletedTask = await taskService.delete(existingTask.id);
      expect(deletedTask).toBeDefined();
      expect(() => taskService.findOne(existingTask.id)).toThrow(NotFoundException);
    });
  });
});
