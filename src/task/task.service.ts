import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { Task } from './task.model';
import { UUID, randomUUID } from 'crypto';

@Injectable()
export class TaskService {
  private tasks: Task[] = [];

  findAll(): Promise<Task[]> {
    return Promise.resolve(this.tasks);
  }

  findOne(id: UUID): Promise<Task> {
    const task = this.tasks.find((task) => task.id === id);
    if (!task) {
      throw new NotFoundException(`Task with id ${id} not found`);
    }
    return Promise.resolve(task);
  }

  create(task): Promise<Task> {
    if (!task.title) {
      throw new BadRequestException('Task title is required');
    }
    const newTask = {
      id: randomUUID(),
      ...task,
      createdAt: new Date(Date.now()),
      completed: false,
      updatedAt: null,
      completedAt: null,
    } as Task;
    this.tasks.push(newTask);
    return Promise.resolve(newTask);
  }

  update(id: UUID, updatedTask: Task): Promise<Task> {
    const taskIndex = this.tasks.findIndex((task) => task.id === id);
    if (taskIndex === -1) {
      throw new NotFoundException(`Task with id ${id} not found`);
    }
    this.tasks[taskIndex] = {
      ...this.tasks[taskIndex],
      ...updatedTask,
      updatedAt: new Date(Date.now()),
      completedAt: updatedTask.completed ? new Date(Date.now()) : null,
    };
    return Promise.resolve(this.tasks[taskIndex]);
  }

  delete(id: UUID) {
    const taskIndex = this.tasks.findIndex((task) => task.id === id);
    if (taskIndex === -1) {
      throw new NotFoundException(`Task with id ${id} not found`);
    }
    this.tasks.splice(taskIndex, 1);
    return Promise.resolve(true);
  }
}
